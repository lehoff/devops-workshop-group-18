import org.junit.Test;
import resources.CalculatorResource;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class CalculatorResourceTest{

    @Test
    public void testCalculate(){
        CalculatorResource calculatorResource = new CalculatorResource();

        String expression = "100+300";
        assertEquals("400", calculatorResource.calculate(expression));

        expression = " 300 - 99 ";
        assertEquals("201", calculatorResource.calculate(expression));

        expression = "5*2";
        assertEquals("10", calculatorResource.calculate(expression));

        expression = "16/4";
        assertEquals("4", calculatorResource.calculate(expression));
    }

    @Test
    public void testSum(){
        CalculatorResource calculatorResource = new CalculatorResource();

        String expression = "100+300+100+100";
        assertEquals(600, calculatorResource.sum(expression));

        expression = "300+100";
        assertEquals(400, calculatorResource.sum(expression));
    }

    @Test
    public void testSubtraction(){
        CalculatorResource calculatorResource = new CalculatorResource();

        String expression = "999-100-100-100";
        assertEquals(699, calculatorResource.subtraction(expression));

        expression = "202-2";
        assertEquals(200, calculatorResource.subtraction(expression));
    }

    @Test
    public void testMultiplication(){
        CalculatorResource calculatorResource = new CalculatorResource();

        String expression = "2*3*2*10";
        assertEquals(120, calculatorResource.multiplication(expression));

        expression = "5*5";
        assertEquals(25, calculatorResource.multiplication(expression));
    }

    @Test
    public void testDivision(){
        CalculatorResource calculatorResource = new CalculatorResource();

        String expression = "100/2/2";
        assertEquals(25, calculatorResource.division(expression));

        expression = "5/5";
        assertEquals(1, calculatorResource.division(expression));
    }

    @Test
    public void testFeedback() {
        CalculatorResource calculatorResource = new CalculatorResource();

        String expression = "dgvd/sds";
        assertEquals("Please enter a number" ,calculatorResource.calculate(expression));
    }
}
